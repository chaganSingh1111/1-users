const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

//Find all users who are interested in playing video games

    const usersWithVideoGame = Object.keys(users)
    .filter((user) => { 
        return users[user].interests && users[user].interests.includes("Video Games")
    })
    console.log(usersWithVideoGame);


//Find all users staying in Germany.

const usersInGermany = Object.keys(users)
  .filter((user) => {
   return users[user].nationality === "Germany"
  })

  console.log(usersInGermany)




  //Sort users based on their seniority level 
  //for Designation - Senior Developer > Developer > Intern
  //for Age - 20 > 10





  //Find all users with masters Degree.
  const usersWihMaster = Object.keys(users)
  .filter((user) => {
   return users[user].qualification === "Masters"
  })

  console.log(usersWihMaster)


  //Group users based on their Programming language mentioned in their designation.
  
    const ProgrammingLanguage = Object.keys(users)
    .filter((user)=>{
        let data={}
        data.javascript = users[user].desgination.includes("Javascript")
        data.python = users[user].desgination.includes("Python")
        data.golang = users[user].desgination.includes("Golang")
        console.log(data)
        return data
    })
    console.log(ProgrammingLanguage)